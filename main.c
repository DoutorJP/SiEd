#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void add(FILE*, char*, char*);
void print(FILE*, char*);
int main(int argc, char** argv)
{
  char* cmd = (char*)malloc(sizeof(char*));
  FILE* fp = fopen(argv[1], "a+");
  while(1){
	printf("> ");
	fgets(cmd, sizeof(cmd), stdin);
	if(cmd[0] == 'q')
	  exit(0);
	else if(cmd[0] == 'p'){
	  print(fp, argv[1]);
	}
	else if(cmd[0] == 'a')
	  add(fp, cmd, argv[1]);
	
	  }
fclose(fp);
return 0;
}

void add(FILE* f, char* str, char* name){
  while(strcmp(str, ".")!= 0){
  printf(": ");	
  scanf("%[^\n]s", str);
  getchar();
  fflush(stdin);
  f = fopen(name, "a+");
  fputs(str, f);
  fputc('\n', f);
  fclose(f);
  }

}  

void print(FILE* f, char* name){
  f = fopen(name, "r");
  char ch;
  int i = 1;
  do {
	ch = fgetc(f);
	if(ch == '\n'){
	  printf("\t%d ", i);
	  i++;
	}
	printf("%c", ch);
  }
  while(ch != EOF);
  putchar('\n');
  fclose(f);
}
